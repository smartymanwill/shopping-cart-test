Shopping Cart Project
======================

Build a shopping cart app based on the mockup images found in `mockups/*`. This project most likely contains more tasks than you will have time to complete, so don't worry if you don't finish. Just complete what you can.

Important notes:
-----------------------

1. Use the mockup images (see `mockups/*`) for guidance on style and layout. Since we're limited on time, don't worry about matching the exact spacing/sizing/colors/etc in your project.
2. Feel free to use the existing components (see `src/components/*`), or write your own if that's easier.
3. As you work on the project, you are free to ask us questions and use any internet resources (e.g. stack overflow, language documentation, etc.) as long as you do your own work.
4. You are also welcome to install 3rd party packages.
5. To view the development build of your project, install your dependencies by running `npm i` (should have already been done), and then run `npm run start` from the project directory, and point your browser to `http://localhost:1234`.

Instructions:
-------------------------


**Step 1. Display a list of products available for purchase**
   
1. Use the pokemon array found in `src/data/pokemon.js`
2. Make an API request to `https://pokeapi.co/api/v2/pokemon/:pokemon_id` for each item in the array (where `:pokemon_id` is the `id` from the pokemon array) to get the pokemon name and image url (found in the `sprites.front_default` attribute of the API response).
3. Add styling to products (see mockups in `mockups/*`). Again, for purposes of this project, the exact styles aren't critical, so long as the general idea comes across.

**Step 2. Track cart information and display shopping cart**

1. Add functionality to the "Add to cart" button.
2. Update cart display (including order total) as new items are added to cart.
3. Add styling to the cart (see mockups).

**Step 3. Add Polish**

1. If you have extra time, this is your opportunity to shine. Implement any changes you think would improve the user experience, the layout/design, functionality, code quality (including pre-existing code), etc. Surprise us with your creative genius!
