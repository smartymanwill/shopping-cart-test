import "./App.scss";

import React from "react";
import Products from "./components/Products.js";
import Cart from "./components/Cart.js";

export const App = () => {
	return (
		<div className={"app"}>
			<h1 className={"page-title"}>Store</h1>
			<Products/>
			<Cart/>
		</div>
	);
};
