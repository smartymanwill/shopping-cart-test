const pokemon = [
	{
		id: 1,
		price: 100,
	},
	{
		id: 6,
		price: 150,
	},
	{
		id: 8,
		price: 30,
	},
	{
		id: 25,
		price: 420,
	},
	{
		id: 39,
		price: 15,
	},
];

export default pokemon;